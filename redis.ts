const redis = require('redis')

export const client = redis.createClient({
  host: 'cache',
  port: 6379,
})

client.on('error', err => {
  console.log(`Error: ${err}`)
})

client.set('course', 'drone')
client.get('course', (err, reply) => {
  console.log(`course value is ${reply}`)
})

client.quit()
